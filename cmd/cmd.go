package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"html/template"
	"io/ioutil"
	"log"
	"strings"

	yaml "gopkg.in/yaml.v2"
)

var cmd Command

type config struct {
	Repos []string `yaml:"repo"`
}

type outJSON struct {
	Repos []outRepoJSON `json:"repo"`
}

type outRepoJSON struct {
	Name     string `json:"name"`
	Terminal string `json:"terminal"`
	Status   string `json:"status"`
}

const (
	SUCCESS = "success"
	FAILED  = "failed"
)

func main() {
	isReportOnly := flag.Bool("report-only", false, "generate report only")
	flag.Parse()

	if !(*isReportOnly) {
		mainRun()
	}
	mainDashboard()
}

func mainRun() {
	// Declare var/const
	cmd.Verbose(false)
	var outRes outJSON
	outRes.Repos = make([]outRepoJSON, 0)

	// Getting Yaml
	var conf config

	data, err := ioutil.ReadFile("../config.yaml")
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	err = yaml.Unmarshal([]byte(data), &conf)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	// Get, Test, Build, and Lint
	var outRepo outRepoJSON

	cmd.Verbose(true)
	for _, repo := range conf.Repos {
		var out string
		out, _ = cmd.Run("go", "get", "-v", repo)
		outRepo.Status = SUCCESS
		outRepo.Name += repo
		outRepo.Terminal += out

		cmd.Verbose(false)
		packsStr, err := cmd.Run("go", "list", repo+"/...")
		outRepo.Terminal += packsStr
		if err != nil {
			outRepo.Status = FAILED
			outRes.Repos = append(outRes.Repos, outRepo)
			log.Printf("error: %v", err)
			continue
		}

		packs := strings.Split(packsStr, "\n")
		cmd.Verbose(true)
		for _, pack := range packs {
			if pack == "" {
				continue
			}
			out, err = cmd.Run("go", "test", pack)
			outRepo.Terminal += out
			if err != nil {
				outRepo.Status = FAILED
				log.Printf("error: %v", err)
				continue
			}

			out, err = cmd.Run("go", "build", pack)
			outRepo.Terminal += out
			if err != nil {
				outRepo.Status = FAILED
				log.Printf("error: %v", err)
				continue
			}

			out, err = cmd.Run("golint", pack)
			outRepo.Terminal += out
			if err != nil {
				outRepo.Status = FAILED
				log.Printf("error: %v", err)
				continue
			}
		}
		outRes.Repos = append(outRes.Repos, outRepo)
	}

	// Save to file
	js, err := json.Marshal(outRes)
	if err != nil {
		log.Printf("error: %v", err)
	}

	err = ioutil.WriteFile("output.json", js, 0644)
	if err != nil {
		log.Printf("error: %v", err)
	}
}

func mainDashboard() {
	// Getting json data
	var data outJSON
	js, err := ioutil.ReadFile("output.json")
	if err != nil {
		log.Printf("error: %v", err)
	}

	err = json.Unmarshal(js, &data)
	if err != nil {
		log.Printf("error: %v", err)
	}

	// Setup Template
	tmpl, err := template.ParseFiles("dashboard_template.html")
	if err != nil {
		log.Printf("error: %v", err)
	}

	// Execute Template
	var buff bytes.Buffer
	err = tmpl.Execute(&buff, map[string]interface{}{"Data": data})
	if err != nil {
		log.Printf("error: %v", err)
	}

	// Generate html files
	err = ioutil.WriteFile("dashboard.html", []byte(buff.String()), 0644)
	if err != nil {
		log.Printf("error: %v", err)
	}

}
